<html>
<head>
    <meta charset="UTF-8">
    <title>My Site</title>
    <script src="static/jquery.js"></script>
    <script src="static/js.js"></script>
    <link rel="stylesheet" href="static/style.css">
</head>
<body>
<div class="flex-container">
    <div class="header">
        <?php include "logo.inc.php" ?>
        <?php include "menu.inc.php" ?>
    </div>
    <div class="body_container">
        <div class="row">
            <div id='panel_content'>
                <div class="panel_left" id="panel_left">
                    <div class="internal_row">
                        <div style='width: 40%'>Name</div>
                        <div style='width: 20%'>Size</div>
                        <div>Time</div>
                    </div>
                    <div>
                        <?php include "dir_content.inc.php" ?>
                    </div>
                </div>
            </div>
            <div class="panel_right right_panel_border show" id="panel_right">
                <div class="internal_row">
                    <div>Name</div>
                    <div>Size</div>
                    <div>Time</div>
                </div>
                <div>
                    <?php include "dir_content.inc.php" ?>
                </div>
            </div>
            <?php include "vars.inc.php" ?>
            <div class="Me right_panel_border" id="MeText">

                <div><img src="dirs/home/of.jpg">
                    <?php echo $about_me ?>
                </div>
            </div>

            <div class="Site right_panel_border" id="SiteText">
                <?php echo $about_site ?>
            </div>
        </div>
    </div>
</div>

<?php echo '<div id="result">' . $dirs . '</div>'; ?>
</body>
</html>