/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function MenuShow() {
    document.getElementById("Menu_border").classList.toggle("show");
}

function FileShow() {
    document.getElementById("File_border").classList.toggle("show");
}

function AboutShow() {
    document.getElementById("About_border").classList.toggle("show");
}

$(document).ready(function () {
    $('.list_element').click(function () {
        $.post("index.php", {dir: $("#result").text() + '/' + $(this).text()}).done(
            function (data) {
                var content = $(data).find("#panel_left");
                var dir = $(data).find("#result").text();
                alert(content)
                $("#panel_content").empty().append(content);
                $("#result").empty().append(dir);
            });
    })
});

// Close the dropdown menu if the user clicks outside of it

window.onclick = function (event) {
    if (!event.target.matches(".dropbtn")) {
        var dropdowns = document.getElementsByClassName("menu_border");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }

    var meTextStatus = document.getElementById("MeText").classList.contains("show")
    var siteTextStatus = document.getElementById("SiteText").classList.contains("show")
    var rightPanelStatus = document.getElementById("panel_right").classList.contains("show")
    if (event.target.getAttribute("id") == "Me") {
        document.getElementById("MeText").classList.toggle("show")
        meTextStatus = document.getElementById("MeText").classList.contains("show")
        document.getElementById("SiteText").classList.remove("show")
        if (meTextStatus == false) {
            document.getElementById("panel_right").classList.add("show")
        } else {
            document.getElementById("panel_right").classList.remove("show")
        }

    }

    if (event.target.getAttribute("id") == "Site") {
        document.getElementById("SiteText").classList.toggle("show");
        document.getElementById("MeText").classList.remove("show");
        siteTextStatus = document.getElementById("SiteText").classList.contains("show")
        if (siteTextStatus == false) {
            document.getElementById("panel_right").classList.add("show");
        } else {
            document.getElementById("panel_right").classList.remove("show")
        }


    }

    if (event.target.getAttribute("id") == "Exit") {
        var win = window.open('http://google.com/', '_blank');
        win.focus();
    }

    // if (event.target.getAttribute("id") == "list_element") {
    //     var test = $(this).find("div")
    //     alert(test)
    // }
}

